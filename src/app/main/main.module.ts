import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {MainRoutingModule} from './main-routing.module';

import {MainComponent} from './main.component';
import {ButtonDirective} from '../shared/direvtives/button.directive';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  imports:[CommonModule, SharedModule, MainRoutingModule],
  declarations:[MainComponent, ButtonDirective, HeaderComponent, SidebarComponent, FooterComponent]

})
export class MainModule {

}
