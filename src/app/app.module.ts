import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import 'rxjs/Rx';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {AuthModule} from './auth/auth.module';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {CommonModule} from '@angular/common';
import {UsersService} from './shared/services/users.service';
import {AuthService} from './shared/services/auth.service';
import {MainModule} from './main/main.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AuthModule,
    HttpClientModule,
    AppRoutingModule,
    CommonModule,
    MainModule

  ],
  providers: [
    UsersService,
    AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
