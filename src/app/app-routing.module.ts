import {NgModule} from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import {AuthComponent} from './auth/auth.component';
import {LoginComponent} from './auth/login/login.component';
import {RegistrationComponent} from './auth/registration/registration.component';
import {MainComponent} from './main/main.component';


const routes: Routes = [
  {path: '', component: AuthComponent, children:[
      {path: '' , redirectTo: 'login', pathMatch: 'full'},
      {path: 'login', component: LoginComponent},
      {path: 'registration', component: RegistrationComponent}
    ]},
  {path: '', component: MainComponent, children:[

    ] }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
