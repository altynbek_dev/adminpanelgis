import {User} from '../models/user.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root' /* замена регистрации в апп модуль */
})

export class AuthService {

  constructor(private http: HttpClient){}

  private isAuth = false


  login(user: User): Observable<User>{
    return this.http.get<User>('http://localhost:3000/users')
  }

  register(user: User): Observable<User>{
   return this.http.post<User>('http://localhost:3000/users', user)
  }

  logout() {
    this.isAuth = false
    window.localStorage.clear()
  }

    isloggedIn(): boolean {
    return this.isAuth
    }

}
