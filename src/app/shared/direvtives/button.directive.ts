import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appButton]'
})

export class ButtonDirective {

  @HostBinding('class.open') isOpen = false

  @HostListener('click') onClick(){
    this.isOpen = !this.isOpen
  }

}
